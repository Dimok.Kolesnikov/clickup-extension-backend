module.exports = function () {
  return (error, req, res, next) => {
    res.status(setErrorStatus(error.status));
    res.json(createError(error));
  }
};

function createError(error) {
  const errorObject = {
    error: {
      http_code: setErrorStatus(error.status),
      code: error.code ?? 0,
      title: error.title ?? 'Error',
      message: error.message ?? '',
    }
  }

  if (error.advanced) {
    errorObject.error.advanced = error.advanced;
  }
  return errorObject;
}

function setErrorStatus(status) {
  return status ?? 500;
}