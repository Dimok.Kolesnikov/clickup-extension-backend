const EmployeeService = require('../services/employeeService/EmployeeService.js');
const CsvService = require('../services/CsvService');
const fields = require('../constants/employee/fields.js');

class EmployeeController {
  async create(req, res, next) {
    req.data = await EmployeeService.create(req.body);
    next();
  }

  async getAll(req, res, next) {
    const employees = await EmployeeService.getAll(req.body);
    req.data = {items: employees};
    next();
  }

  async getSpecific(req, res, next) {
    req.data = await EmployeeService.getSpecific(req.params.id);
    next();
  }

  async update(req, res, next) {
    req.data = await EmployeeService.update(req.body);
    return next();
  }

  async remove(req, res, next) {
    await EmployeeService.remove(req.params.id);
    req.data = {id: req.params.id};
    next();
  }

  async importCSV(req, res, next) {
    const {file} = req;
    const {lastName, firstName, middleName, department, level, timePerWeek} = fields;
    const columnNames = [lastName, firstName, middleName, department, level, timePerWeek];
    const config = {isRemoveFirstRow: true};
    const parsedEmployees = await CsvService.parseCSV(file, columnNames, 'employees', config);
    await EmployeeService.addByArray(parsedEmployees);
    req.data = 'success';
    next();
  }

  async exportCSV(req, res, next) {
    const employees = await EmployeeService.getAll().map(employee => ({...employee.dataValues}));
    const fields = [
      {key: fields.lastName, as: 'Фамилия*'},
      {key: fields.firstName, as: 'Имя*'},
      {key: fields.middleName, as: 'Отчество'},
      {key: fields.department, as: 'Отдел*'},
      {key: fields.level, as: 'Уровень*'},
      {key: fields.timePerWeek, as: 'Количество часов в неделю*'},
    ];
    const filePath = await CsvService.createCSV(employees, fields, 'employees');
    req.data = {url: filePath};
    next();
  }

  exportCSVExample(req, res, next) {
    const filePath = CsvService.getCSVPath('employeesExample');
    req.data = {url: filePath};
    next();
  }
}

module.exports = new EmployeeController();