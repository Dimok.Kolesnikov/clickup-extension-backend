module.exports = function () {
  return (req, res, next) => {
    if (req.route) {
      res.status(createSuccess(req));
      res.json({data: req.data});
    } else {
      next();
    }
  };
}

function createSuccess(req) {
  const {get, post, put} = methods;
  switch (req.method) {
    case get.name:
      return get.httpStatus;
    case post.name:
      return post.httpStatus;
    case put.name:
      return put.httpStatus;
    case methods.delete.name:
      return methods.delete.httpStatus;
    default: {
      return 200;
    }
  }
}

const methods = {
  get: {
    name: 'GET',
    httpStatus: 200,
  },
  post: {
    name: 'POST',
    httpStatus: 201,
  },
  put: {
    name: 'PUT',
    httpStatus: 202,
  },
  delete: {
    name: 'DELETE',
    httpStatus: 203,
  },
}