const fs = require('fs');
const path = require('path');
const createError = require('http-errors');
const csvParse = require('csv-parse');
const ObjectToCsv = require('object-to-csv');

class CsvService {
  async parseCSV (file, columnNames, csvName, config) {
    this._csvFileValidation(file);
    const pathToCsv = path.resolve('temporaryFiles', `${csvName}.csv`);
    config = config ? config : {};
    return new Promise(resolve => {
      const result = [];
      fs.createReadStream(pathToCsv)
        .on('data', data => {
          csvParse(data, {
            columns: columnNames,
            delimiter: [',', ';'],
          }, (err, output) => {
            if (output) {
              result.push(...output);
            }
          });
        })
        .on('end', () => {
          if (config.isRemoveFirstRow) {
            result.splice(0, 1);
          }
          fs.unlinkSync(pathToCsv);
          resolve(result);
        });
    });
  }

  createCSV (array, columnNames, csvName) {
    const csv = new ObjectToCsv({
      keys: columnNames, data: array,
    }).getCSV();
    const filePath = path.resolve('static', `${csvName}.csv`);
    fs.writeFileSync(filePath, csv);
    return filePath;
  }

  getCSVPath(csvName) {
    return path.resolve('static', `${csvName}.csv`);
  }

  _csvFileValidation(file) {
    if (!file) {
      throw createError(422, 'Файл обязателен');
    }
    const {mimetype} = file;
    if (mimetype !== 'text/csv') {
      throw createError(422, 'Файл должен быть формата csv');
    }
  }
}

module.exports = new CsvService();