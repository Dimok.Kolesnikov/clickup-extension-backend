const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const unknownRoute = require('./middlewares/unknownRoute');
const errorHandler = require('./middlewares/errorHandler');
const successHandler = require('./middlewares/successHandler');

const app = express();
const employeesRoute = require('./routes/employeeRoute');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.static('static'));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', employeesRoute);

app.use(successHandler());
app.use(unknownRoute());

app.use(errorHandler());

module.exports = app;
