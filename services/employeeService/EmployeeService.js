const {Op} = require('sequelize');
const createError = require('http-errors');
const {Employee} = require('../../models');
const fields = require('../../constants/employee/fields');
const testValidationError = require('../../utils/testValidationErrors');

class EmployeeService {
  async create(employee) {
    try {
      return await this._create(employee);
    } catch (e) {
      throw createError(422, 'Данные неверны', {data: testValidationError(e.errors)});
    }
  }

  _create(employee) {
    return Employee.create(employee);
  }

  getAll(params) {
    return Employee.findAll({where: {...params}});
  }

  getSpecific(id) {
    return Employee.findOne({where: {id}});
  }

  async update(params) {
    const {id, firstName, lastName, middleName, department, level, timePerWeek} = params;
    const employeeParams = {};
    if (id) {
      employeeParams.id = id;
    } else {
      throw createError(400, 'id is required');
    }
    if (firstName) {
      employeeParams.firstName = firstName;
    }
    if (lastName) {
      employeeParams.lastName = lastName;
    }
    if (middleName) {
      employeeParams.middleName = middleName;
    }
    if (department) {
      employeeParams.department = department;
    }
    if (level) {
      employeeParams.level = level;
    }
    if (timePerWeek) {
      employeeParams.timePerWeek = timePerWeek;
    }
    const employee = await Employee.findOne({where: {id,}});
    if (!employee) {
      throw createError(404, 'Пользователя с таким id не существует');
    }
    try {
      return await this._update(employee, employeeParams);
    } catch (e) {
      throw createError(422, 'Данные неверны', {data: testValidationError(e.errors)});
    }
  }

  _update(employee, employeeParams) {
    Object.entries(employeeParams).forEach(([key, value]) => {
      employee[key] = value;
    });
    return employee.save();
  }

  async remove(id, operator) {
    if ((id === undefined || id === null) && !operator) {
      throw createError(400, 'id is required');
    }
    if (id) {
      const employee = await Employee.findOne({where: {id}});
      if (employee) {
        await employee.destroy()
      } else {
        throw createError(404, 'Сотрудника с таким id не существует');
      }
    } else {
      await Employee.destroy({where: {id: operator}})
    }
  }

  async addByArray(employees) {
    function validationErrorProcessing(errors, rowNumber,) {
      return Object.fromEntries(errors.map(validationError => {
        const {message, path} = validationError;
        return [path, {message, rowNumber}];
      }));
    }

    const formattedEmployees = employees.map(employee => this.format(employee));
    const errors = [];
    const employeesId = [];
    let rowNumber = 1;
    for (let employee of formattedEmployees) {
      const {firstName, lastName} = employee;
      const [foundEmployee] = await this.getAll({firstName, lastName});
      try {
        if (foundEmployee) {
          await this._update(foundEmployee, employee);
          employeesId.push(foundEmployee.id);
        } else {
          const createdEmployee = await this._create(employee);
          employeesId.push(createdEmployee.id);
        }
      } catch (e) {
        errors.push(validationErrorProcessing(e.errors, rowNumber));
      }
      rowNumber += 1;
    }
    if (errors.length) {
      throw createError(422, 'Данные неверны', {advanced: errors});
    } else {
      await this.remove(null, {
        [Op.notIn]: employeesId,
      });
    }
  }

  format(employee) {
    return Object.fromEntries(Object.entries(employee).map(([key, value]) => {
      if (value) {
        switch (key) {
          case fields.firstName:
          case fields.lastName:
          case fields.middleName:
            return [key, value[0].toUpperCase() + value.toLowerCase().slice(1)];
          case fields.department:
          case fields.level:
            return [key, value.toLowerCase()];
          default:
            return [key, value];
        }
      } else {
        return [key, value];
      }
    }));
  }
}

module.exports = new EmployeeService();