const multer = require('multer');
const employeeStorageOptions = require('./employeeStorageOptions.js');
const employeeStorage = multer.diskStorage(employeeStorageOptions);
const uploadEmployee = multer({storage: employeeStorage});
module.exports = {uploadEmployee};