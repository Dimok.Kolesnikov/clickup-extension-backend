module.exports = {
  firstName: 'firstName',
  lastName: 'lastName',
  middleName: 'middleName',
  department: 'department',
  level: 'level',
  timePerWeek: 'timePerWeek',
}