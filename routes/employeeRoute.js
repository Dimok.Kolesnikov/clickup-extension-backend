const express = require('express');
const router = express.Router();
const ash = require('express-async-handler');
const {uploadEmployee} = require('../constants/multerStorage');
const {
  create,
  getAll,
  getSpecific,
  update,
  remove,
  importCSV,
  exportCSV,
  exportCSVExample,
} = require('../controllers/EmployeeController')

router.post('/employees', ash(create));
router.route('/employees/csv').post(uploadEmployee.single('employeesFile'), ash(importCSV));
router.get('/employees/csv', ash(exportCSV));
router.get('/employees/csv-example', ash(exportCSVExample));
router.get('/employees', ash(getAll));
router.get('/employees/:id', ash(getSpecific));
router.put('/employees', ash(update));
router.delete('/employees/:id', ash(remove));

module.exports = router;
