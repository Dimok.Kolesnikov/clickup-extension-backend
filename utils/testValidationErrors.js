module.exports = function (errorsArray) {
  return errorsArray.map(error => ({[error.path]: error.message}));
}