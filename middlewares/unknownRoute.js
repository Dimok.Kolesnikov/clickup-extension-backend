const createError = require('http-errors');

module.exports = function () {
  return (req, res, next) => {
    next(createError(404));
  }
};