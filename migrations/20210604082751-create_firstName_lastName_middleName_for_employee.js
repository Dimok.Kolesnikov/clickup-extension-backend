'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      await queryInterface.addColumn(
        'Employees',
        'firstName',
        {type: Sequelize.DataTypes.STRING}
      ),
      await queryInterface.addColumn(
        'Employees',
        'lastName',
        {type: Sequelize.DataTypes.STRING}
      ),
      await queryInterface.addColumn(
        'Employees',
        'middleName',
        {type: Sequelize.DataTypes.STRING}
      ),
      await queryInterface.removeColumn('Employees', 'fullName')
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      await queryInterface.removeColumn('Employees', 'firstName'),
      await queryInterface.removeColumn('Employees', 'lastName'),
      await queryInterface.removeColumn('Employees', 'middleName'),
      await queryInterface.addColumn(
        'Employees',
        'fullName',
        {type: Sequelize.DataTypes.STRING})
    ])
  }
};
