const app = require('./app');
const {Sequelize} = require('sequelize');

const dbConnect = {
  username: "Dmitry",
  password: "12345",
  database: "clickup_extension",
  host: "127.0.0.1",
  dialect: "postgres",
  port: "5001"
}

async function startApp () {
  try {
    const listeningPort = 5000;
    const sequelize = new Sequelize(dbConnect);
    await sequelize.authenticate();
    app.listen(listeningPort, () => {
      console.log('Server start listening on port', listeningPort);
    })
  } catch (error) {
    console.log(error);
  }
}

startApp();