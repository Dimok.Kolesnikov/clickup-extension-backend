const path = require('path');
const storageOptions = {
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '/../../temporaryFiles'));
  },
  filename: function (req, file, cb) {
    cb(null, 'employees' + '.csv')
  }
}

module.exports = storageOptions;