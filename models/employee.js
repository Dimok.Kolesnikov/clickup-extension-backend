'use strict';
const {Model} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Employee extends Model {
  };
  Employee.init({
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        is: {
          args: ["^[а-яА-Яё]*$"],
          msg: 'Имя должно содержать только буквы кириллического алфавита',
        },
        notNull: {
          msg: 'Имя обязательно для заполнения',
        },
        len: {
          args: [3, 99],
          msg: "Имя должно быть более 2 и менее 100 символов"
        }
      }
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        is: {
          args: ["^[а-яА-Яё]*$"],
          msg: 'Фамилия должна содержать только буквы кириллического алфавита',
        },
        notNull: {
          msg: 'Фамилия обязательна для заполнения',
        },
        len: {
          args: [3, 99],
          msg: "Фамилия должна быть более 2 и менее 100 символов"
        }
      }
    },
    middleName: {
      type: DataTypes.STRING,
      allowNull: true,
      allowEmpty: true,
      validate: {
        is: {
          args: ["^[а-яА-Яё]*$"],
          msg: 'Отчество должно содержать только буквы кириллического алфавита',
        },
        len: {
          args: [0, 100],
          msg: 'Отчество не должно быть более 100 символов',
        }
      }
    },
    department: {
      type: DataTypes.STRING,
      validate: {
        isIn: {
          args: [['frontend', 'backend', 'project manager']],
          msg: 'Значение поля Отдел может быть только одним из: frontend, backend, project manager',
        },
      }
    },
    level: {
      type: DataTypes.STRING,
      validate: {
        isIn: {
          args: [['junior', 'middle', 'senior']],
          msg: 'Значение поля Уровень может быть только одним из: junior, middle, senior',
        },
      }
    },
    timePerWeek: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: {
          args: [[20, 40]],
          msg: 'Количество часов за неделю может быть равно только: 20, 40',
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Employee',
  });
  return Employee;
};